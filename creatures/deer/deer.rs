use std::{thread, time::Duration};

fn main() {
    
    let mut tree_size: u32 = 50;
    
    struct deer_1 {
        hunger: u32,
        speed: u32,
        strength: u32,
        health: u32,
        current_tree: u8
    }

    struct deer_2 {
        hunger: u32,
        speed: u32,
        strength: u32,
        health: u32,
        current_tree: u8
    }
    static deer_1: deer_1 = deer_1 {hunger: 10, speed: 10, strength: 10, health: 10, current_tree: 1}
    static deer_2: deer_2 = deer_2 {hunger: 10, speed: 10, strength: 10, health: 10, current_tree: 1}
    
    do_game_tick();
}

// This function will feed the animal and trigger the tree territorial sequence.
fn eat() {

    if (tree_size <= 20) {
        stats.hunger += 20;
    } else if (tree_size >= 20) && (tree_size <= 50) {
        stats.hunger += 40;
    } else if (tree_size >= 50) && (tree_size <= 100) {
        stats.hunger += 80;
    } else {
        //EMPTY
    } 

    if (current_tree == current_tree_for_2) {
        protect_tree();
    }
}

// This function will make the deer attack any other animal feeding on the same tree.
fn protect_tree() {
    //TODO
}

// This function will handle the predator attacking escape.
fn under_attack() {
    //TODO
}

// This will update the game state.
fn do_game_tick() {

    if (rng.gen_range(0..51) == 1) {
        stats.hunger -= 10;
    }

    if (rng.gen_range(0..51) == 1) {
        stats.health += 10;
    }

    if (stats.hunger <= 10) {
        eat();
    }

}