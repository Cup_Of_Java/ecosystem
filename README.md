# Ecosystem
The simulation of evolution and natural beauty.
## Contributing
Ask for access to the repository and you will be accepted, anyone is welcome to the project as long as they help.
If you have a suggestion then just put it in an issue and the team will more than likely consider it.
## Download
There is currently no way to run the simulation except for copying and pasting the code directly into a code editor.
This will be adressed soon.