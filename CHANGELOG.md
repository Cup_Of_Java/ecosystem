# Changelog

# V0.01

## Added
- Added hunger system.
- Added different food amounts for different sizes of trees.
- Added deer defending the tree they are feeding on.
- Added hunger consumption.
- Added health regen
## Changed

## Removed